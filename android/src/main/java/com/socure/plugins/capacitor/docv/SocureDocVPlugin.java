package com.socure.plugins.capacitor.docv;

import com.getcapacitor.JSArray;
import com.getcapacitor.JSObject;
import com.getcapacitor.PermissionState;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.ActivityCallback;
import com.getcapacitor.annotation.CapacitorPlugin;
import com.getcapacitor.annotation.Permission;
import com.getcapacitor.annotation.PermissionCallback;
import com.socure.docv.capturesdk.api.SocureDocVHelper;
import com.socure.docv.capturesdk.common.utils.ResultListener;
import com.socure.docv.capturesdk.common.utils.ScanError;
import com.socure.docv.capturesdk.common.utils.ScannedData;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.util.Base64;

import androidx.activity.result.ActivityResult;
import androidx.annotation.NonNull;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CapacitorPlugin(
        name = "SocureDocV",
        permissions = {
                @Permission(
                        alias = SocureDocVPlugin.CAMERA,
                        strings = {Manifest.permission.CAMERA}
                )
        }
)
public class SocureDocVPlugin extends Plugin {
    // Permission alias constants
    static final String CAMERA = "camera";

    public static final String TAG = "SocureDocVPlugin";
    public static final String SOCURE_API_KEY = "socureApiKey";
    public static final String SOCURE_FLOW = "flow";
    private static final String PERMISSION_DENIED_ERROR_CAMERA = "User denied access to camera";

    private PluginCall currentCall;

    @Override
    @PluginMethod
    public void requestPermissions(PluginCall call) {
        // If the camera permission is defined in the manifest, then we have to prompt the user
        // or else we will get a security exception when trying to present the camera. If, however,
        // it is not defined in the manifest then we don't need to prompt and it will just work.
        if (isPermissionDeclared(CAMERA)) {
            // just request normally
            super.requestPermissions(call);
        } else {
            // the manifest does not define camera permissions, so we need to decide what to do
            // first, extract the permissions being requested
            JSArray providedPerms = call.getArray("permissions");
            List<String> permsList = null;
            try {
                permsList = providedPerms.toList();
            } catch (JSONException e) {
            }

            if (permsList != null && permsList.size() == 1 && permsList.contains(CAMERA)) {
                // the only thing being asked for was the camera so we can just return the current state
                checkPermissions(call);
            }
        }
    }

    @Override
    public Map<String, PermissionState> getPermissionStates() {
        Map<String, PermissionState> permissionStates = super.getPermissionStates();

        // If Camera is not in the manifest and therefore not required, say the permission is granted
        if (!isPermissionDeclared(CAMERA)) {
            permissionStates.put(CAMERA, PermissionState.GRANTED);
        }

        return permissionStates;
    }

    private boolean checkCameraPermissions(PluginCall call) {
        // if the manifest does not contain the camera permissions key, we don't need to ask the user
        boolean needCameraPerms = isPermissionDeclared(CAMERA);
        boolean hasCameraPerms = !needCameraPerms || getPermissionState(CAMERA) == PermissionState.GRANTED;
        if (!hasCameraPerms) {
            requestPermissionForAlias(CAMERA, call, "cameraPermissionsCallback");
            return false;
        }
        return true;
    }

    /**
     * Completes the plugin call after a camera permission request
     *
     * @param call the plugin call
     */
    @PermissionCallback
    private void cameraPermissionsCallback(PluginCall call) {
        if (getPermissionState(CAMERA) != PermissionState.GRANTED) {
            call.reject(PERMISSION_DENIED_ERROR_CAMERA);
        }
    }

    @PluginMethod
    public void launchSocureDocV(PluginCall call) {
        String socureApiKey = call.getString(SOCURE_API_KEY);
        if (socureApiKey == null) {
            call.reject("A Socure API key is required!");
            return;
        }

        String flow = call.getString(SOCURE_FLOW);

        startActivityForResult(
                call,
                SocureDocVHelper.INSTANCE.getIntent(getActivity(), socureApiKey, flow),
                "socureActivityResult"
        );
    }

    @ActivityCallback
    private void socureActivityResult(PluginCall call, ActivityResult result) {
        currentCall = call;
        if (call == null) {
            return;
        }

        Intent data = result.getData();
        if (data == null) {
            call.reject("Failed to receive data from activity!");
            return;
        }

        int resultCode = result.getResultCode();
        if (resultCode == Activity.RESULT_OK) {
            SocureDocVHelper.INSTANCE.getResult(
                    data,
                    new ResultListener() {
                        @Override
                        public void onSuccess(@NonNull ScannedData scannedData) {
                            if (currentCall != null) {
                                currentCall.resolve(scannedDataToJSObject(scannedData));
                            }
                        }

                        @Override
                        public void onError(@NonNull ScanError scanError) {
                            if (currentCall != null) {
                                currentCall.reject(
                                        scanError.getErrorMessage(),
                                        String.valueOf(scanError.getStatusCode()),
                                        null,
                                        scanErrorToJSObject(scanError)
                                );
                            }
                        }
                    }
            );
        } else {
            call.reject("Failed to launch SocureDocV!");
        }
    }

    private JSObject scannedDataToJSObject(@NonNull ScannedData scannedData) {
        JSObject docVResponse = new JSObject();
        docVResponse.put("docUUID", scannedData.getDocUUID());
        docVResponse.put("sessionId", scannedData.getSessionId());

        JSObject captureData = new JSObject();
        if (scannedData.getCaptureData() != null) {
            for (Map.Entry<String, String> entry : scannedData.getCaptureData().entrySet()) {
                captureData.put(entry.getKey(), entry.getValue());
            }
        }
        docVResponse.put("captureData", captureData);

        docVResponse.put(
                "capturedImages",
                capturedImagesToJSObject(scannedData.getCapturedImages())
        );

        if (scannedData.getExtractedData() != null) {
            try {
                docVResponse.put(
                        "extractedData",
                        new JSObject(scannedData.getExtractedData())
                );
            } catch (JSONException ex) {
                docVResponse.put("extractedData", scannedData.getExtractedData());
            }
        }

        return docVResponse;
    }

    @NonNull
    private JSObject capturedImagesToJSObject(HashMap<String, byte[]> capturedImages) {
        JSObject capturedImagesObj = new JSObject();
        if (capturedImages != null) {
            for (Map.Entry<String, byte[]> entry : capturedImages.entrySet()) {
                capturedImagesObj.put(
                        entry.getKey(),
                        Base64.encodeToString(entry.getValue(), Base64.DEFAULT)
                );
            }
        }
        return capturedImagesObj;
    }

    private JSObject scanErrorToJSObject(@NonNull ScanError scanError) {
        JSObject scanErrorResponse = new JSObject();
        scanErrorResponse.put("errorMessage", scanError.getErrorMessage());
        scanErrorResponse.put("statusCode", scanError.getStatusCode());
        scanErrorResponse.put("sessionId", scanError.getSessionId());
        scanErrorResponse.put(
                "capturedImages",
                capturedImagesToJSObject(scanError.getCapturedImages())
        );

        return scanErrorResponse;
    }
}
