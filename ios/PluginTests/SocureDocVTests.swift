import XCTest
import Capacitor
@testable import Plugin

class SocureDocVTests: XCTestCase {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testLaunchSocureDocV() {
      let expectation = self.expectation(description: "SocureDocV method call expectation")
      let call = CAPPluginCall(callbackId: "testCallbackId", options: ["socureApiKey": "asdf1234"], success: { (result, call) in
                  // Handle success
                  expectation.fulfill()
              }, error: { (error) in
                  // Handle error
                  expectation.fulfill()
              })

      let implementation = SocureDocVPlugin()
      implementation.launchSocureDocV(call!)

      XCTAssertTrue(true)
    }
}
