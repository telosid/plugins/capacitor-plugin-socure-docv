import Capacitor
import SocureDocV

@objc(SocureDocVPlugin)
class SocureDocVPlugin: CAPPlugin {
  let objDocVHelper = SocureDocVHelper()
  @objc func launchSocureDocV(_ call: CAPPluginCall) -> Void {
      guard let socureApiKey = call.getString("socureApiKey") else {
        call.reject("socureApiKey is a required parameter!")
        return
      }
      let flow = call.getString("flow")
      var flowConfig: [String: Any]?
      do {
          flowConfig = try flow?.convertToDictionary()
      } catch {
          call.reject("Invalid config data")
          return
      }
      objDocVHelper.setSource(.reactNative)
      DispatchQueue.main.async {
          let root = self.bridge?.viewController;
          self.objDocVHelper.launch(socureApiKey, presentingViewController: root!, config: flowConfig, completionBlock: { result in
              switch result {
              case .success(let scan):
                  call.resolve(scan.dictionary)
                  break
              case .failure(let error):
                  call.reject(
                    error.errorMessage,
                    String(error.statusCode),
                    nil,
                    error.dictionary
                  );
                  break
              }
          })
      }
  }

  @objc static func requiresMainQueueSetup() -> Bool {
    return false
  }
}

extension String {
    func convertToDictionary() throws -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                throw error
            }
        }
        return nil
    }
}
