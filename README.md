# capacitor-plugin-socure-docv

An Ionic Capacitor wrapper for accessing the native Socure scanner SDKs

# Example project


## Telos ID NPM Registry

```bash
echo @telosid:registry=https://gitlab.com/api/v4/projects/29462567/packages/npm/ >> .npmrc
```

## iOS Setup
Install `cocoapods-user-defined-build-types`.

Since the Socure Document Verification SDK is an XCFramework, CocoaPods doesn’t easily allow dynamic
frameworks to intermingle with static libraries. This gem modifies CocoaPods to allow both to exist
at the same time. Follow the instructions
at https://github.com/joncardasis/cocoapods-user-defined-build-types.

* Edit your `ios/App/Podfile`
  * Set your target version to `13.0`
    ```
      platform :ios, '13.0'
    ```
  * Add the Socure iOS SDK cocoapod
    ```
      pod 'SocureDocV', '3.1.1'
    ```
Your `ios/App/Podfile` should look like:
```
platform :ios, '13.0'
use_frameworks!

# workaround to avoid Xcode caching of Pods that requires
# Product -> Clean Build Folder after new Cordova plugins installed
# Requires CocoaPods 1.6 or newer
install! 'cocoapods', :disable_input_output_paths => true

def capacitor_pods
  pod 'Capacitor', :path => '../../node_modules/@capacitor/ios'
  pod 'CapacitorCordova', :path => '../../node_modules/@capacitor/ios'
  pod 'CapacitorApp', :path => '../../node_modules/@capacitor/app'
  pod 'CapacitorHaptics', :path => '../../node_modules/@capacitor/haptics'
  pod 'CapacitorKeyboard', :path => '../../node_modules/@capacitor/keyboard'
  pod 'CapacitorStatusBar', :path => '../../node_modules/@capacitor/status-bar'
  pod 'TelosidCapacitorPluginSocureDocv', :path => '../../../capacitor-plugin-socure-docv'
end

target 'App' do
  capacitor_pods
  # Add your Pods here
  pod 'SocureDocV', '3.1.1'
end

```

* Edit your `ios/App/App/Info.plist`
  * Add `NSCameraUsageDescription` and `socurePublicKey` entries to the bottom of the dictionary
```
  <key>NSCameraUsageDescription</key>
  <string>This application requires the use of your phone's camera</string>
  <key>socurePublicKey</key>
  <string>replace_with_your_public_key</string>
</dict>
</plist>
```

## Android Setup
* Edit `android/variables.gradle` and set `minSdkVersion` to 22
```
minSdkVersion = 22
```
Your `android/variables.gradle` should look like this:
```
ext {
    minSdkVersion = 22
    compileSdkVersion = 32
    targetSdkVersion = 32
    androidxActivityVersion = '1.4.0'
    androidxAppCompatVersion = '1.4.2'
    androidxCoordinatorLayoutVersion = '1.2.0'
    androidxCoreVersion = '1.8.0'
    androidxFragmentVersion = '1.4.1'
    junitVersion = '4.13.2'
    androidxJunitVersion = '1.1.3'
    androidxEspressoCoreVersion = '3.4.0'
    cordovaAndroidVersion = '10.1.1'
}
```

* Edit `android/build.gradle` 
  * add `jitpack` maven repository into the `allprojects.repositories` section
    ```
    maven { url 'https://www.jitpack.io' }
    ```
  * add the `kotlin-gradle-plugin` to the `dependencies`
    ```
    classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.31"
    ```

Your `android/build.gradle` Should look like this:
```
// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {

  repositories {
    google()
    mavenCentral()
  }
  dependencies {
    classpath 'com.android.tools.build:gradle:4.2.1'
    classpath 'com.google.gms:google-services:4.3.5'
    classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.31"
    // NOTE: Do not place your application dependencies here; they belong
    // in the individual module build.gradle files
  }
}

apply from: "variables.gradle"

allprojects {
  repositories {
    google()
    mavenCentral()
    maven { url 'https://www.jitpack.io' }
  }
}

task clean(type: Delete) {
  delete rootProject.buildDir
}
```

* Add `kotlin-stdlib` to your `android/app/build.gradle` `dependencies`
```
implementation "org.jetbrains.kotlin:kotlin-stdlib:1.3.61"
```
Your `android/app/build.grade` `dependencies` should look like:
```
dependencies {
  implementation fileTree(include: ['*.jar'], dir: 'libs')
  implementation "androidx.appcompat:appcompat:$androidxAppCompatVersion"
  implementation project(':capacitor-android')
  testImplementation "junit:junit:$junitVersion"
  androidTestImplementation "androidx.test.ext:junit:$androidxJunitVersion"
  androidTestImplementation "androidx.test.espresso:espresso-core:$androidxEspressoCoreVersion"
  implementation project(':capacitor-cordova-android-plugins')
  implementation "org.jetbrains.kotlin:kotlin-stdlib:1.3.61"
}
```

## Install

```bash
npm install @telosid/capacitor-plugin-socure-docv
npx cap sync
```
### Add your Socure public key to the environment
* Edit `src/environments/environment.ts` and export your `socurePublicKey`
```typescript
export const environment = {
  production: false,
  socurePublicKey: ''
};
```

## API

<docgen-index>

* [`checkPermissions()`](#checkpermissions)
* [`requestPermissions()`](#requestpermissions)
* [`launchSocureDocV(...)`](#launchsocuredocv)
* [Interfaces](#interfaces)
* [Type Aliases](#type-aliases)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### checkPermissions()

```typescript
checkPermissions() => Promise<PermissionStatus>
```

**Returns:** <code>Promise&lt;<a href="#permissionstatus">PermissionStatus</a>&gt;</code>

--------------------


### requestPermissions()

```typescript
requestPermissions() => Promise<PermissionStatus>
```

**Returns:** <code>Promise&lt;<a href="#permissionstatus">PermissionStatus</a>&gt;</code>

--------------------


### launchSocureDocV(...)

```typescript
launchSocureDocV(options: SocureDocVOptions) => Promise<ScannedData>
```

| Param         | Type                                                            |
| ------------- | --------------------------------------------------------------- |
| **`options`** | <code><a href="#socuredocvoptions">SocureDocVOptions</a></code> |

**Returns:** <code>Promise&lt;<a href="#scanneddata">ScannedData</a>&gt;</code>

--------------------


### Interfaces


#### PermissionStatus

| Prop         | Type                                                        |
| ------------ | ----------------------------------------------------------- |
| **`camera`** | <code><a href="#permissionstate">PermissionState</a></code> |


#### ScannedData

| Prop                 | Type                                                      |
| -------------------- | --------------------------------------------------------- |
| **`docUUID`**        | <code>string</code>                                       |
| **`sessionId`**      | <code>string</code>                                       |
| **`captureData`**    | <code><a href="#map">Map</a>&lt;string, string&gt;</code> |
| **`capturedImages`** | <code><a href="#map">Map</a>&lt;string, string&gt;</code> |
| **`extractedData`**  | <code><a href="#map">Map</a>&lt;string, string&gt;</code> |


#### Map

| Prop       | Type                |
| ---------- | ------------------- |
| **`size`** | <code>number</code> |

| Method      | Signature                                                                                                      |
| ----------- | -------------------------------------------------------------------------------------------------------------- |
| **clear**   | () =&gt; void                                                                                                  |
| **delete**  | (key: K) =&gt; boolean                                                                                         |
| **forEach** | (callbackfn: (value: V, key: K, map: <a href="#map">Map</a>&lt;K, V&gt;) =&gt; void, thisArg?: any) =&gt; void |
| **get**     | (key: K) =&gt; V \| undefined                                                                                  |
| **has**     | (key: K) =&gt; boolean                                                                                         |
| **set**     | (key: K, value: V) =&gt; this                                                                                  |


#### SocureDocVOptions

| Prop               | Type                |
| ------------------ | ------------------- |
| **`socureApiKey`** | <code>string</code> |
| **`flow`**         | <code>string</code> |


### Type Aliases


#### PermissionState

<code>'prompt' | 'prompt-with-rationale' | 'granted' | 'denied'</code>

</docgen-api>
