import { WebPlugin } from '@capacitor/core';

import type { SocureDocVPlugin, PermissionStatus } from './definitions';

export class SocureDocVWeb extends WebPlugin implements SocureDocVPlugin {
  async checkPermissions(): Promise<PermissionStatus> {
    throw this.unimplemented('Not implemented on web.');
  }

  async requestPermissions(): Promise<PermissionStatus> {
    throw this.unimplemented('Not implemented on web.');
  }

  async launchSocureDocV(): Promise<any> {
    throw this.unimplemented('Not implemented on web.');
  }
}
