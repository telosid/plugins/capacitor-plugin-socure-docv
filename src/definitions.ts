import type { PermissionState } from '@capacitor/core';

export interface PermissionStatus {
  camera: PermissionState;
}

export interface SocureDocVPlugin {
  // echo(options: { value: string }): Promise<{ value: string }>;

  checkPermissions(): Promise<PermissionStatus>;

  requestPermissions(): Promise<PermissionStatus>;

  launchSocureDocV(options: SocureDocVOptions): Promise<ScannedData>;
}

export interface SocureDocVOptions {
  socureApiKey: string;
  flow?: string;
}

export interface ScannedData {
  docUUID: string;
  sessionId: string;
  captureData: Map<string, string>;
  capturedImages: Map<string, string>;
  extractedData: Map<string, string>;
}