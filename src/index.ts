import { registerPlugin } from '@capacitor/core';

import type { SocureDocVPlugin } from './definitions';

const SocureDocV = registerPlugin<SocureDocVPlugin>('SocureDocV', {
  web: () => import('./web').then(m => new m.SocureDocVWeb()),
});

export * from './definitions';
export { SocureDocV };
